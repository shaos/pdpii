/*
  pdpiirom.c - utility to make ROM image from 4 separate binary files

  Created by Alexander "Shaos" Shabarshin <me@shaos.net> on March 18, 2019

  THIS PROGRAM IS PUBLIC DOMAIN - USE IT ON YOUR OWN RISC!
*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
 int i,k,c,s;
 int ffs,nop;
 FILE *f,*fo;
 ffs = 0;
 nop = 0;
 if(argc<2)
 {
    printf("\nPDPiiROM is utility to make ROM image from 4 separate binary files\n\n");
    printf("Usage: pdpiirom newrom.bin [rom1.bin rom2.bin rom3.bin rom4.bin]\n\n");
    printf("Instead of input filename you can use keywords FF or NOP to generate blank or NOP-image\n\n");
    return -1;
 }
 fo = fopen(argv[1],"wb");
 if(fo==NULL)
 {
    printf("\nError: Can't create output file '%s'\n\n",argv[1]);
    return -2;
 }
 for(k=0;k<4;k++)
 {
    f = NULL;
    ffs = nop = s = 0;
    if(k+2 >= argc) ffs = 1;
    else if(argv[k+2][0]=='F' && argv[k+2][1]=='F' && argv[k+2][2]==0) ffs = 1;
    else if(argv[k+2][0]=='N' && argv[k+2][1]=='O' && argv[k+2][2]=='P' && argv[k+2][3]==0) nop = 1;
    else
    {
        f = fopen(argv[k+2],"rb");
        if(f==NULL)
        {
           fclose(fo);
           printf("\nError: Can't open input file '%s'\n\n",argv[k+2]);
           return -3;
        }
        fseek(f,0,SEEK_END);
        s = ftell(f);
        fseek(f,0,SEEK_SET);
    }
    if(nop) printf("Bank %i: NOP image\n",k);
    if(ffs) printf("Bank %i: Blank image\n",k);
    if(f) printf("Bank %i: ROM image %s with size %i\n",k,argv[k+2],s);
    for(i=0;i<16384;i++)
    {
        if(ffs)
        {
           fputc(0xFF,fo);
           fputc(0xFF,fo);
        }
        else if(nop)
        {
           /* NOP is 0240 or 0 000 000 010 100 000 = 0x00A0 (inversion is 0xFF5F) */
           fputc(0x5F,fo);
           fputc(0xFF,fo);
        }
        else
        {
           /* invert every byte of input file */
           if(i+i>=s) c = 0;
           else c = fgetc(f);
           fputc((~c)&0xFF,fo);
           if(i+i+1>=s) c = 0;
           else c = fgetc(f);
           fputc((~c)&0xFF,fo);
        }
    }
    if(f!=NULL) fclose(f);
 }
 fclose(fo);
 return 0;
}
