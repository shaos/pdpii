; Written by Shaos on 19-MAR-2019 to test PDPii with 16x2 LCD

; to compile use https://gitlab.com/shaos/pdp11asm85

; php11asm85 lcd-test1.asm

; THIS PROGRAM IS PUBLIC DOMAIN - USE IT ON YOUR OWN RISC!

decimalnumbers
    org 0
    .word 0100H,0E0H ; reset vector
    org 0100H
start:
    mov #12500,r0 ; delay 15 ms for 20 MHz
delay0:
    sob r0,delay0
    mov #init,r4
initloop:
    movb (r4)+,r0
    beq endinit
    com r0 ; invert character
    bis #0FF00H,r0 ; set all bits of higher byte
    mov r0,@#0FF00H
    bic #800H,r0 ; inverted E=1
    mov r0,@#0FF00H
    bis #800H,r0 ; inverted E=0
    mov r0,@#0FF00H
    mov #4167,r0 ; delay 5 ms for 20 MHz
delay1:
    sob r0,delay1
    jmp initloop
endinit:
    mov #msg,r4
loop:
    movb (r4)+,r0
    beq endloop
    com r0 ; invert character
    bis #0FF00H,r0 ; set all bits of higher byte
    bic #200H,r0 ; inverted RS=1
    mov r0,@#0FF00H
    bic #800H,r0 ; inverted E=1
    mov r0,@#0FF00H
    bis #800H,r0 ; inverted E=0
    mov r0,@#0FF00H
    mov #833,r0 ; delay 1 ms for 20 MHz
delay2:
    sob r0,delay2
    jmp loop
endloop:
    mov #0FFFFH,r0
delay:
    sob r0,delay
    jmp start

msg:	.byte	"Hello, World!!!",0
init:	.byte	30H,30H,38H,8H,1H,6H,0CH,0

make_mk85_rom "lcd-test1.rom", 32768
